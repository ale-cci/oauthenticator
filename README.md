## OAuthenticator
> Microservice for managing OAuth2 authentication


### OAuth2 Endpoints
- `/auth`
    - GET request handles code and implicit grants
    - POST request hadles client credentials + client password
- `/userinfo`
    - GET request only, returns detiled informations about the authenticated user (according to token grants)
- `/token`
    - Token exchange endpoint
- `/urls`
    - List of endpoints
- `/.well-known/jwks.json`
    - Available PUBLIC keys


### API for Service management
- creating a new user
- changing user data
- changing authorizations for an existing user
- delete an user
- create a new authorization
- get all users with a specific authorization
- create a new client


### Functions
- register-endpoints
- connect to database
- start the server

#### NOTE:
Trying out the following project structure:
> https://www.wolfe.id.au/2020/03/10/how-do-i-structure-my-go-project/

Taking inspiration from this talk:
> https://medium.com/@matryer/how-i-write-go-http-services-after-seven-years-37c208122831


#### Contribute
See [./CONTRIBUTING.md]()
