### Contributing to this repository
- add tests to your pull requests
- don't submit pull requests with failing tests



#### Strongly suggested
Please use the following formalism for commit messages
* https://www.conventionalcommits.org/en/v1.0.0/
* https://chris.beams.io/posts/git-commit/
* https://gist.github.com/qoomon/5dfcdf8eec66a051ecd85625518cfd13


###### Summary:
* `feat` : commit that add a new feature
* `fix` : commit that fixes a bug
* `refactor`: rewrite/restructure code and does not alter the original behaviour
* `perf`: special refactors for performance improvement
* `style`: white-space, formatting, missing semicolons, typos...
* `test`: add missing tests or correct existing tests
* `docs`: changes only the documentation
* `build`: changes to build tools/pipeline, project version or other things that affect the building process
* `ops`: operational commits like infrastructure, deployment, backup, recovery...
* `chore`: miscellaneous, like modifiying the `.gitignore`
