FROM golang:1.16.2-alpine3.12 as base
RUN apk add build-base

WORKDIR /root
COPY go.mod go.sum ./
RUN go mod download

COPY cli cli
COPY pkg pkg

COPY ./templates/ ./templates
RUN go test ./...
CMD go run ./cli/oauthsrv.go

FROM base as builder
RUN go build ./cli/oauthsrv.go

FROM alpine:3.12 as prod
RUN adduser --disabled-password web web
USER web

WORKDIR /home/web
COPY --from=builder --chown=web:web /root/oauthsrv .
CMD /home/web/oauthsrv
