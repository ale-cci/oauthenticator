package main

import (
	"ale-cci/oauthsrv/pkg/app"
	"ale-cci/oauthsrv/pkg/router"
	"log"
	"net/http"
)

func main() {
    application, err := app.CreateApp()

    if err != nil {
        log.Panicf("Failed app intialization: %q", err)
    }

    r := router.Router{}
    application.AddRoutes(&r)

    log.Printf("Start serving on :7300")
    if err := http.ListenAndServe(":7300", &r); err != nil {
        log.Printf("Failed to listen: %q", err)
    }
}
