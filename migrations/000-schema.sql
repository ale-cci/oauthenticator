create table o2_accounts (
    id int(12) auto_increment,
    username varchar(36) not null,
    password varchar(512) not null default "",
    status enum("active", "suspended") not null,

    primary key(id),
    unique (username)
);

 insert into o2_accounts (`username`, `password`) values
('admin', 'sha256$1234$d84464181f7f019f3fb10e6bbd0543d7ac84c4f8e360ebb9402a472ab30ebc');

-- create table o2_claims (
--     id
--     name
--     description
-- );
--
--
-- create table o2_groups (
-- );
--
--
-- create table o2_external_account (
--     id_user
--     website
--     provider_id
-- );
