package crypto

import (
	"strings"
	"crypto/sha256"
	"fmt"
	"errors"
)

const (
	HashAlgSHA256 = "sha256"
)


// Compare plaintext password with a hashed password.
// The input hashed password takes the form of:
//     algorithm $ salt $ hash
//
// Currently supported algorithms are: sha256
func ValidatePassword(password, saltedPassword string) error {
	chunks := strings.Split(saltedPassword, "$")
	if len(chunks) != 3 {
		return errors.New("Malformed password hash")
	}

	algorithm, salt := chunks[0], chunks[1]
	newPasswd, err := HashWithSalt(algorithm, salt, password)

	if err != nil {
		return fmt.Errorf("Unable to calculate hash: %w", err)
	}

	if newPasswd != saltedPassword {
		return errors.New("Mismatching password")
	}
	return nil
}


// Create a valid hashed password using the provided hash, algorith, salt and
// plaintext.
func HashWithSalt(algorithm, salt, plaintext string) (string, error) {
	var hashString string

	if algorithm == HashAlgSHA256 {
		hashVal := sha256.Sum256([]byte(salt + plaintext))
		hashString = fmt.Sprintf("%x", hashVal)
	} else {
		msg := fmt.Sprintf("Algorithm %q doesn't match any hashing function", algorithm)
		return "-", errors.New(msg)
	}

	return fmt.Sprintf("%s$%s$%s", algorithm, salt, hashString), nil
}
