package crypto_test

import (
	"testing"
	"ale-cci/oauthsrv/pkg/crypto"
)


func TestValidatePassword(t *testing.T) {
	t.Run("should authenitcate without salt", func(t *testing.T) {
		hashed := "sha256$$5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8"
		err := crypto.ValidatePassword("password", hashed)
		if err != nil {
			t.Errorf("Unable to confirm password: %q", err)
		}
	})

	t.Run("should return error if password is wrong", func(t *testing.T) {
		hashed := "sha256$$5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8"
		err := crypto.ValidatePassword("not-password", hashed)
		if err == nil {
			t.Error("Expected error during authenitcation: nil returned")
		}
	})

	t.Run("should return unupported algorithm, before checking the password", func(t *testing.T) {
		hashed := "missing$$--"
		err := crypto.ValidatePassword("", hashed)

		if err == nil {
			t.Errorf("Expected error, got %q", err.Error())
		}
	})

	t.Run("should return error if password has wrong number of chunks", func(t *testing.T) {

		tt := []string{"a$b", "a$b$c$d"}
		expect := "Malformed password hash"

		for _, testCase := range(tt) {
			err := crypto.ValidatePassword("", testCase)
			got := err.Error()

			if got != expect {
				t.Errorf("On input %q: Expected %q, got %q", testCase, expect, got)
			}
		}
	})
}


func TestHashAlgorithm(t *testing.T) {
	t.Run("should return error when hashing algorithm is wrong", func(t *testing.T) {
		_, err := crypto.HashWithSalt("missing", "", "")
		if err == nil {
			t.Errorf("Expected error, got nil")
		}
	})
}


func TestIntegration(t *testing.T) {
	t.Run("HashWithSalt should generate password validable with ValidatePassword", func(t *testing.T) {
		hash, err := crypto.HashWithSalt(crypto.HashAlgSHA256, "", "plaintext-password")
		if err != nil {
			t.Fatalf("Unable to generate password hash: %v", err)
		}

		err = crypto.ValidatePassword("plaintext-password", hash)
		if err != nil {
			t.Fatalf("Generated password is invalid: %v", err)
		}
	})
}
