package router_test

import (
	"ale-cci/oauthsrv/pkg/router"
	"net/http"
	"net/http/httptest"
	"testing"
)

func responseCounter(counter *int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		*counter += 1
	}
}

func TestRouterStruct(t *testing.T) {
	t.Run("should call request handler", func(t *testing.T) {
		var called int
		handlerFn := responseCounter(&called)

		r := router.Router{}
		r.AddEndpoint("GET", "/test", handlerFn)

		req := httptest.NewRequest("GET", "/test", nil)
		w := httptest.NewRecorder()

		r.ServeHTTP(w, req)

		if called != 1 {
			t.Errorf("Handler called %d times instead of just 1", called)
		}
	})

	t.Run("should not call the function when method is different", func(t *testing.T) {

		var called int
		handlerFn := responseCounter(&called)

		r := router.Router{}
		r.AddEndpoint("GET", "/test", handlerFn)

		req := httptest.NewRequest("POST", "/test", nil)
		w := httptest.NewRecorder()

		r.ServeHTTP(w, req)
		expect := 0

		if called != expect {
			t.Errorf("Handler called %d times, %d expected", called, expect)
		}
	})

	t.Run("should return method not allowed when method is different", func(t *testing.T) {
		handlerFn := func(w http.ResponseWriter, r *http.Request) {}

		r := router.Router{}
		r.AddEndpoint("GET", "/test", handlerFn)

		req := httptest.NewRequest("POST", "/test", nil)
		w := httptest.NewRecorder()

		r.ServeHTTP(w, req)

		expect := http.StatusMethodNotAllowed
		got := w.Result().StatusCode
		if got != expect {
			t.Errorf("Wrong status code %d, expected %d", got, expect)
		}
	})

	t.Run("should be able to register multiple handlers with different methods", func(t *testing.T) {
		var getCalled int
		var postCalled int
		getHandlerFn := responseCounter(&getCalled)
		postHandlerFn := responseCounter(&postCalled)

		r := router.Router{}
		r.AddEndpoints("/test", []router.Endpoint{
			{"GET", getHandlerFn},
			{"POST", postHandlerFn},
		})

		req := httptest.NewRequest("POST", "/test", nil)
		w := httptest.NewRecorder()

		r.ServeHTTP(w, req)

		if getCalled != 0 {
			t.Errorf("GET method called %d times instead of 0", getCalled)
		}

		if postCalled != 1 {
			t.Errorf("POST method called %d times instead of 1", postCalled)
		}
	})
}
