package router

import "net/http"

type Router struct {
	router http.ServeMux
}

func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	r.router.ServeHTTP(w, req)
}

func (r *Router) AddEndpoint(method string, pattern string, handler http.HandlerFunc) {
	r.AddEndpoints(pattern, []Endpoint{{method, handler}})
}

type Endpoint struct {
	Method  string
	Handler http.HandlerFunc
}

func (r *Router) AddEndpoints(pattern string, endpoints []Endpoint) {
	wrapper := func(w http.ResponseWriter, req *http.Request) {
		req.ParseForm()
		for _, e := range endpoints {
			if e.Method == req.Method {
				e.Handler(w, req)
				return
			}
		}

		w.WriteHeader(http.StatusMethodNotAllowed)
	}

	r.router.HandleFunc(pattern, wrapper)
}
