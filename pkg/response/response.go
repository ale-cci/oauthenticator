package response

import (
	"fmt"
	"net/http"
	"encoding/json"
)

type HttpResponse struct {
	Status int `json:"-"`
	Message string `json:"message"`
}

func (h *HttpResponse) WriteTo(w http.ResponseWriter) {
	body, err := json.Marshal(h)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Add("Content-Type", "application/json")

	w.WriteHeader(h.Status)
	w.Write(body)
}


type HttpResponseWriter interface {
	WriteTo(w http.ResponseWriter)
}


func MissingField(field string) *HttpResponse {
	return &HttpResponse {
		http.StatusBadRequest,
		fmt.Sprintf("%q field is missing or empty", field),
	}
}


type Arg struct {
	Name string
	Location *string
}

type Getter interface {
	Get(name string) string
}

func GetOrError(g Getter, args ...Arg) *HttpResponse {
	for _, arg := range args {
		value := g.Get(arg.Name)
		if value == "" {
			return MissingField(arg.Name)
		}
		*arg.Location = value
	}
	return nil
}
