package app

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
)

type App struct {
	db *sql.DB
}

// Pass database connection as parameter
func CreateApp( /* config, db */ ) (*App, error) {
	conn, err := connect()
	if err != nil {
		return nil, errors.Wrap(err, "Failed app initialization:")
	}

	app := App{
		conn,
	}
	return &app, nil
}



func connect() (*sql.DB, error) {
	connStr := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s",
		os.Getenv("MYSQL_USER"),
		os.Getenv("MYSQL_PASS"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_DB"),
	)

	conn, err := sql.Open("mysql", connStr)
	if err != nil {
		return nil, errors.Wrap(err, "Unable connect using env credentials:")
	}

	conn.SetMaxIdleConns(10)
	conn.SetMaxOpenConns(10)
	conn.SetConnMaxLifetime(3 * time.Minute)

	return conn, nil
}
