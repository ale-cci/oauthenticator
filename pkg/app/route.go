package app

import (
	"ale-cci/oauthsrv/pkg/router"
)

func (app *App) AddRoutes(r *router.Router) {
	r.AddEndpoints("/", []router.Endpoint{
		{ Method: "GET", Handler: app.handleIndex() },
	})

	r.AddEndpoints("/login", []router.Endpoint{
		{ Method: "GET", Handler: app.handleRenderLogin() },
		{ Method: "POST", Handler: app.handleLogin() },
	})
}


