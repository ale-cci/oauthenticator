package app

import (
	"ale-cci/oauthsrv/pkg/crypto"
	"database/sql"
	"html/template"
	"net/http"
	"log"

	"github.com/pkg/errors"
	"ale-cci/oauthsrv/pkg/httpflash"
)

// Login page
func (app *App) handleRenderLogin() http.HandlerFunc {
	type TemplateData struct {
		ErrorMessage string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		t, err := template.ParseFiles("templates/login.tmpl")

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Read cookies
		msg, err := httpflash.GetFlash(w, r, "msg")
		data := TemplateData{}

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		data.ErrorMessage = string(msg)

		if err := t.Execute(w, data); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}


// Check login credentials
func (app *App) handleLogin() http.HandlerFunc {
	type UserData struct {
		id int
		password string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		username := r.PostFormValue("username")
		password := r.PostFormValue("password")

		// redirect_uri := r.PostFormValue("redirect_uri")
		// scope := r.PostFormValue("scope")
		// grant_type := r.PostFormValue("grant_type")
		// client_id := r.PostFormValue("client_id

		log.Printf("%s %s", username, password)
		var userData UserData
		err := app.db.QueryRowContext(
			r.Context(),
			"select id, password from o2_accounts where username = ?",
			username,
		).Scan(&userData.id, &userData.password)


		if err != nil {
			if errors.Cause(err) == sql.ErrNoRows {
				httpflash.SetFlash(w, "msg", []byte("Wrong credentials"))
				http.Redirect(w, r, "/login", http.StatusFound)
				return
			}

			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}

		if err := crypto.ValidatePassword(password, userData.password); err != nil {
			httpflash.SetFlash(w, "msg", []byte("Wrong credentials"))
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		http.SetCookie(w, &http.Cookie{Name: "sid", Value: "1234"})
		http.Redirect(w, r, "/home", http.StatusTemporaryRedirect)
	}
}


func (app *App) handleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/login", http.StatusFound)
	}
}
