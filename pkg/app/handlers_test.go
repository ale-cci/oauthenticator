package app

import (
	"ale-cci/oauthsrv/pkg/crypto"
	"database/sql"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

func init() {
	// required for loading tempaltes relative to project root
	if err := os.Chdir("../../"); err != nil {
		panic(err)
	}
}

func openSqlConn(t *testing.T) *sql.DB {
	os.Remove("./test.db")
	sqliteDB, err := sql.Open("sqlite3", "./test.db")
	if err != nil {
		t.Fatalf("Unable to create sqlite database: %v", err)
	}

	pw1, _ := crypto.HashWithSalt(crypto.HashAlgSHA256, "", "one")
	pw2, _ := crypto.HashWithSalt(crypto.HashAlgSHA256, "", "two")
	pw3, _ := crypto.HashWithSalt(crypto.HashAlgSHA256, "", "three")

	_, err = sqliteDB.Exec(fmt.Sprintf(`
		create table o2_accounts (
			id int auto_increment,
			username varchar(36) not null,
			password varchar(512) not null default "",
			status char default 'A' not null,

			primary key(id),
			unique (username)
		);

		insert into o2_accounts
		(id, username, password)
		values
		(1, "user1", %q),
		(2, "user2", %q),
		(3, "user3", %q);
	`, pw1, pw2, pw3))

	if err != nil {
		t.Fatalf("Unable to init schema: %v", err)
	}

	return sqliteDB
}


func TestHandleAuth(t *testing.T) {
	app := App{openSqlConn(t)}
	handler := app.handleLogin()
	ts := httptest.NewServer(handler)

	tt := []struct {
		Name     string
		Username     string
		Password     string
		ExpectedCode int
		ExpectFlash string
	}{
		{
			Name: "should return message with error credentials are not found",
			Username: "test-username", Password: "test-password",
			ExpectedCode: http.StatusFound,
			ExpectFlash: "msg",
		},
		{
			Name: "should return message with error credentials if password is wrong",
			Username: "user1", Password: "test-password",
			ExpectedCode: http.StatusFound,
			ExpectFlash: "msg",
		},
		{
			Name: "should redirect to home with code if username and password are correct",
			Username: "user1", Password: "one",
			ExpectedCode: http.StatusTemporaryRedirect,
			ExpectFlash: "",
		},
	}


	client := &http.Client{
		CheckRedirect: noFollowRedirectPolicy,
	}

	for index, testData := range tt {
		testName := fmt.Sprintf("[%d] %s", index, testData.Name)
		t.Run(testName, func(t *testing.T) {
			resp, err := client.PostForm(ts.URL, url.Values{
				"username": []string{testData.Username},
				"password": []string{testData.Password},
			})

			if err != nil {
				t.Errorf("An error occurred during http.PostForm: %v", err)
			}

			if resp.StatusCode != testData.ExpectedCode {
				t.Errorf("Expected %d status code, got %d", testData.ExpectedCode, resp.StatusCode)

				defer resp.Body.Close()
				body, err := io.ReadAll(resp.Body)
				if err == nil {
					t.Errorf("Reason: %q", body)
				} else {
					t.Errorf("Unable to read response body")
				}
			}

			if testData.ExpectFlash != "" {
				cookies := resp.Cookies()
				if cookies == nil {
					t.Fatalf("No cookies set by endpoint")
				}

				found := false
				for _, cookie := range cookies {
					if cookie.Name == testData.ExpectFlash {
						found = true
						break
					}
				}

				if !found {
					t.Errorf("Cookie with name %q hasn't been set", testData.ExpectFlash)
				}
			}
		})
	}
}


func TestRenderLogin(t *testing.T) {
	app := App{openSqlConn(t)}
	handler := app.handleRenderLogin()
	ts := httptest.NewServer(handler)

	tt := []struct {
		Name string
		ExpectedStatus int
	}{
		{
			Name: "should render correctly",
			ExpectedStatus: 200,
		},
	}
	client := &http.Client{}

	for idx, testData := range tt {
		testName := fmt.Sprintf("[%d] %s", idx, testData.Name)
		t.Run(testName, func(t *testing.T) {
			resp, err := client.Get(ts.URL)
			if err != nil {
				t.Fatalf("An error occurred during http.Get: %v", err)
			}
			defer resp.Body.Close()

			if resp.StatusCode != testData.ExpectedStatus {
				t.Errorf("Expected response status %d, got %d.", testData.ExpectedStatus, resp.StatusCode)

				if body, err := io.ReadAll(resp.Body); err == nil {
					t.Errorf("Reason: %q", body)
				} else {
					t.Errorf("Unable to read response body: %v", err)
				}
			}
		})
	}

}


func noFollowRedirectPolicy(req *http.Request, via []*http.Request) error {
	return http.ErrUseLastResponse
}


